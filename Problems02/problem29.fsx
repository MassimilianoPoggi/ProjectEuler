﻿let pow (a, b) = (float a)**(float b);;

let couples = List.collect (fun el -> List.map (fun el2 -> (el, el2)) [2..100]) [2..100];

Set.map pow (Set.ofList couples) |> Set.count;;