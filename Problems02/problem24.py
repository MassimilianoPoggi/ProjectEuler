import itertools
result = list(itertools.permutations(range(0, 10)))

def to_number(list):
	number = 0
	for i in range(0, 10):
		number += (10**i) * (list[9-i])
	return number

ordered = list(map(lambda x: to_number(x), result))
ordered.sort()

print(ordered[999999])
