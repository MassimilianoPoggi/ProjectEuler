resource = open("problem22.txt", "r")
text = resource.read()
resource.close()

names = list(map(lambda x: x.replace("\"", ''), text.split(",")))

def name_value(s):
	return sum([ord(c)-ord('A')+1 for c in s.upper()])

names.sort()

result = 0
for i in range(0, len(names)):
	result += (i+1) * name_value(names[i])

print(result)




