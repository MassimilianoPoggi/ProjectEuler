﻿let rec spiral_sum size =
    match size with
        | 1 -> 1
        | n -> 
            let s = spiral_sum (size-2) 
            let m = (size - 2) * (size - 2)
            let sum = s + (m + size-1) + (m + 2* (size-1)) + (m + 3*(size-1)) + (m + 4*(size-1))
            sum;;