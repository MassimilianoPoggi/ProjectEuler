def d(n):
	return sum([x for x in range(1, n//2+1) if n % x == 0])

def is_amicable(n):
	x = d(n)
	return x != n and d(x) == n

print(sum(filter(is_amicable, range(1, 10000))))
