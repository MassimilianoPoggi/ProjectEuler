from functools import reduce

def fact(n):
	return reduce(lambda x,y: x*y, range(1, n+1), 1)

print(sum([int(digit) for digit in str(fact(100))]))
