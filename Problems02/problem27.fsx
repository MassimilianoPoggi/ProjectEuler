﻿let is_prime x =
    let number = if x > 0 then x else - x
    in let rec test_n n =
        if n > (number |> float |> sqrt |> int) then true
        else match number % n with
                | 0 -> false
                | _ -> test_n (n+2)
        in number = 2 || (number % 2 <> 0 && test_n 3);;

let possible_b = List.filter is_prime [-999..999];;
let possible_a = List.filter (fun x -> x % 2 <> 0) [-999..999];;

let count_primes (a, b) =
    let rec find_index index =
        if (is_prime (index*index + a*index + b)) then find_index (index+1)
        else index
    in find_index 0;;

let couples = List.collect (fun el -> List.map (fun el2 -> (el, el2)) possible_b) possible_a;;


(fun (_, (b, c)) -> b*c) (List.max (List.map (fun c -> (count_primes c, c)) couples));;