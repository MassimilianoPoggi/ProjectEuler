﻿let count_recurring_digits x =
    let rec find_double remainders value =
        if (value = 0) then 0
        else
        let newval = value * 10
        let remainder = newval % x
        match List.tryFindIndex (fun el -> el = remainder) remainders with
            | Some(x) -> x+1
            | None -> find_double (remainder::remainders) (remainder)
    in find_double [] 1;;

 List.max (List.zip (List.map count_recurring_digits [1..1000]) [1..1000]);;