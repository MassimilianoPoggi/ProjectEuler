#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <stdint.h>

bool is_abundant(int n) {
	int sum = 0;
	int i = 0;
	for (i = 1; i <= ceil(n/2); i++)
		if (n % i == 0)
			sum+=i;
	
	return sum > n;
}

int main() {
	int abundants[10000] = {0};
	int nums[28124] = {0};
	int i = 0, j = 0;
	int index = 0;
	int64_t result = 0;

	for(i = 0; i < 28124; i++) {
		nums[i] = i;
		if (is_abundant(i)) abundants[index++] = i;
	}

	for(i = 0; i < index; i++) {
		for (j = i; j < index; j++)
			if (abundants[i] + abundants[j] < 28124)
				nums[abundants[i]+abundants[j]] = 0;
	}

	for(i = 0; i < 28124; i++) {
		result += nums[i];
	}

	printf("Result: %d\n", result);
	return 0;
}
