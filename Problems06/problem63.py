flag = True
count = 0
power = 1
while(flag):
	for i in range(9, 0, -1):
		if (len(str(i**power)) == power):
			count += 1
		else:
			if (i == 9):
				flag = False
			break
	power += 1

print(count)
