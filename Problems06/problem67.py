resource = open("problem67.txt", "r")
text = resource.read()
resource.close()

rows = list(filter(lambda x: x, text.split("\n")))
nums = list(map(lambda x: list(map(lambda y: int(y),x.split())), rows))

def find_optimized_row(highrow, lowrow):
	return [max(highrow[i]+lowrow[i], highrow[i]+lowrow[i+1]) for i in range(len(highrow))]

for index in range(len(nums)-2, -1, -1):
	nums[index][:] = find_optimized_row(nums[index], nums[index+1])

print(nums[0][0])
