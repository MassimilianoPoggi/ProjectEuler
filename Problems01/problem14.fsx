﻿let collatz n =
    match n % 2L with
        | 0L -> n / 2L
        | _ -> 3L * n + 1L
    
let collatz_count n =
    let rec collatz_count acc num =
        match num with
            | 1L -> acc
            | x -> collatz_count (acc + 1) (collatz x)
    in collatz_count 0 n;;

List.max (List.zip (List.map collatz_count [1L..999999L]) [1L..999999L]);;