﻿let get_div_num n = 
    ([1L..n |> float |> sqrt |> int64] |> List.filter (fun x -> n % x = 0L) |> List.length) * 2;;
    
let rec find_num n index = 
    if (get_div_num n >= 500) then n
    else find_num (n+index) (index + 1L);;

find_num 1L 2L;;