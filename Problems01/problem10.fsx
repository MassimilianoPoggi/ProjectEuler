﻿let prime_list max =
    let rec prime_list i res =
        match List.item i res with
            | n when n*n > max -> res
            | n -> prime_list (i+1) (List.filter (fun x -> x % n <> 0 || x = n) res)
    in prime_list 0 [2..max];;

List.reduce (+) (List.map int64 (prime_list 1999999));;