﻿let sum_square = ([1..100] |> List.reduce(+) |> float)**2. |> int;;
let square_sum = [1..100] |> List.map (fun x -> (float x) ** 2.) |> List.reduce (+) |> int;;

sum_square - square_sum;;