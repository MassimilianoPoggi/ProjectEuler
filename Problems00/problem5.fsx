﻿let primes = [2;3;5;7;11;13;17;19];;

let expand_primes max_num =
    let rec fits times n =
        if (float n) ** (float times) < float max_num then fits (times + 1) n
        else times - 1
    in List.concat (List.map2 List.replicate ((List.map (fits 2) primes)) (primes));;

List.reduce ( * ) (List.map int64 (expand_primes 20));;