﻿let is_pythagorean (a, b, c) =
    a*a + b*b = c*c;;

[1..1000] 
    |> List.map (fun el1 -> 
        [el1..1000-el1] |> 
        List.map (fun el2 -> 
            (el1, el2, 1000-el1-el2))) 
    |> List.concat 
    |> List.filter is_pythagorean
    |> List.head 
    |> (fun (a,b,c) -> a*b*c);;