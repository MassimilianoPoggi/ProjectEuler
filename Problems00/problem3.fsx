﻿let isPrime (n: int64) =
    let rec findDivisor list =
        match list with
            | [] -> true
            | h::tl when n % h = 0L -> false
            | h::tl -> findDivisor tl
    in findDivisor [2L..n |> float |> sqrt |> int64];;

let maxPrimeFactor (n:int64) =
    let rec maxFactor list =
        match list with
            | [] -> n
            | h::tl when n % h = 0L && isPrime h -> h
            | h::tl -> maxFactor tl
    in maxFactor (List.rev [2L..n |> float |> sqrt |> int64]);;

maxPrimeFactor 600851475143L;;