﻿let fiblist max =
    let rec fib n1 n2 acc =
        if (n1 + n2 < 0) then failwith "overflow!" else
            if (n1 + n2 > max) then acc
            else fib n2 (n1 + n2) ((n1 + n2)::acc)
    in fib 1 2 [2;1];;

List.reduce (+) (List.filter (fun x -> x % 2 = 0) (fiblist 3999999));;