﻿let is_palindrome (n: int) =
    let l = [for c in string n -> c]
    in List.rev l = l;;

let products = [100..999] |> List.map (fun el -> [100..999] |> List.map (fun el2 -> el*el2)) ;; 

products |> List.concat |> List.filter is_palindrome |> List.max;;