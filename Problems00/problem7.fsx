﻿let isPrime (n: int64) =
    let rec findDivisor list =
        match list with
            | [] -> true
            | h::tl when n % h = 0L -> false
            | h::tl -> findDivisor tl
    in findDivisor [2L..n |> float |> sqrt |> int64];;

let find_prime index =
    let rec find_prime i n =
        if (isPrime n) then
            if (i = index - 1 ) then n
            else find_prime (i+1) (n+2L)
        else
            find_prime i (n+2L)
    in find_prime 1 3L;;
