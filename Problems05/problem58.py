#Miller-Rabin primality test
from random import randrange

small_primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31] # etc.

def probably_prime(n, k):
    """Return True if n passes k rounds of the Miller-Rabin 
primality
    test (and is probably prime). Return False if n is proved to 
be
    composite.

    """
    if n < 2: return False
    for p in small_primes:
        if n < p * p: return True
        if n % p == 0: return False
    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in range(k):
        a = randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True


#problem
def is_prime(n):
        return probably_prime(n, 7)




side = 7
diagprimes = 8
diagtotal = 13
while(diagprimes/diagtotal >= 0.1):
	side += 2
	##can exclude the highest one, it's always side*side
	for i in range(1,4):
		if (is_prime((side-2)**2 + i*(side-1))):
			diagprimes += 1

	diagtotal += 4

print(side)
