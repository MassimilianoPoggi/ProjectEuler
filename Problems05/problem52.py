def same_digits(n1, n2):
	return sorted(str(n1)) == sorted(str(n2))

num = 1
while(True):
	if (len(str(num)) != len(str(num*6))):
		num = 10**(len(str(num)))
		continue

	found = True
	for i in range(2, 7):
		if (not same_digits(num, num*i)):
			found = False
			break

	if (found):
		print(num)
		break

	num +=1
