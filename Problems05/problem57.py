from fractions import Fraction


f = 2
count = 0
for i in range(1,1001):
	approx = 1 + Fraction(1,f)
	if (len(str(approx.numerator)) > len(str(approx.denominator))):
		count += 1

	f = 2 + Fraction(1, f)
print(count)
