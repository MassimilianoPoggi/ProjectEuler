lychrels = [True for i in range(0, 10000)]

def reverse(n):
	nstr = str(n)
	return int("".join([nstr[i] for i in range(len(nstr)-1, -1, -1)]))

def palindrome(n):
	return n == reverse(n)

def is_lychrel(n):
	next = reverse(n) + n
	for j in range(0, 50):
		if (palindrome(next)):
			return False
		next = reverse(next)+next
	return True

count = 0
for i in range(10000-1, -1, -1):
	next = reverse(i) + i
	if (palindrome(next)):
		lychrels[i] = False
	else:
		if (next < 10000):
			lychrels[i] = lychrels[next]

		lychrels[i] = is_lychrel(i)

	if (lychrels[i]):
		count += 1

print(count)
