from math import factorial

def choose(n, k):
	return factorial(n)/(factorial(k)*factorial(n-k))

count = 0
for n in range(1, 101):
	for k in range(n//2, 0, -1):
		if (choose(n, k) > 10**6):
			if (k == n//2 and n % 2 != 0):
				count += 1
			else:
				count+=2
		else:
			break

print(count)
