def digital_sum(n):
	sum = 0
	for digit in str(n):
		sum += int(digit)
	return sum

max = 0

for a in range(1, 100):
	for b in range(1, 100):
		res = digital_sum(a**b)
		if (res > max):
			max = res

print(max)
