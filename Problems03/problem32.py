def is_pandigital(a, b, c):
	return ''.join(sorted(str(a)+str(b)+str(c))) == "123456789"


products = []
for a in range(10, 100):
	for b in range(100, 1000):
		if (len(str(a*b)) == 4 and is_pandigital(a, b, a*b)):
			products.append(a*b)

for a in range(1, 10):
	for b in range(1000, 10000):
		if (len(str(a*b)) == 4 and is_pandigital(a, b, a*b)):
			products.append(a*b)

print(sum(set(products)))
