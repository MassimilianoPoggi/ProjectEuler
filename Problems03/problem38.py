def is_pandigital(s):
	return ''.join(sorted(str(s))) == "123456789"

def concat(n):
	return int(''.join(map(lambda x: str(x), n)))

max = 0
for i in range(25, 34):
	num = concat([i, i*2, i*3, i*4])
	if (num > max and is_pandigital(num)):
		max = num

for i in range(100, 334):
	num = concat([i, i*2, i*3])
	if (num > max and is_pandigital(num)):
		max = num

for i in range(5000, 9999):
	num = concat([i, i*2])
	if (num > max and is_pandigital(num)):
		max = num

print(max)
