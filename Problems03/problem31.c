#include <stdio.h>

int split(int value, int* coins) {
	if (*coins == 1) return 1;

	int i = 0, val = 0;
	int count = 0;

	for (i = 0; i <= value / *coins; i++) {
		val = value - *coins * i;
		if (val == 0)
			count++;
		else
			count += split(val, coins+1);
	}

	return count;
}

int main() {
	int count = 0;
	int coins[8] = {200, 100, 50, 20, 10, 5, 2, 1};
	count = split(200, coins);
	printf("%d\n", count);
	return 0;
}
