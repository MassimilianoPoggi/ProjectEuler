def digit_power_sum(n):
	sum = 0
	for c in str(n):
		sum += int(c)**5
	return sum

result = 0
for n in range(2, 9**5 * 5 + 1):
	if n == digit_power_sum(n):
		result += n

print(result)
