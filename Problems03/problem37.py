from math import sqrt

def is_prime(n):
	if n == 1:
		return False
	if n == 2:
		return True

	for i in range(2, int(sqrt(n))+1):
		if (n % i == 0):
			 return False
	return True

def is_truncatable(n):
	num = str(n)
	for i in range(1, len(num)):
		uptoi = int(num[:i])
		fromi = int(num[i:])
		if ((not is_prime(uptoi)) or (not is_prime(fromi))):
			return False
	return True

found = 0
result = 0
curr = 0
def check_middle_digits(str):
	for c in str[1:-1]:
		if c not in ['1', '3', '7', '9']:
			return False
	return True

while (found < 11):
	curr += 10
	curr_as_str = str(curr)
	if (curr_as_str[0] not in ['2', '3', '5', '7']):
		continue

	if (not check_middle_digits(curr_as_str)):
		continue

	if (is_prime(curr+3) and is_truncatable(curr+3)):
		result += curr+3
		found += 1

	if (is_prime(curr+7) and is_truncatable(curr+7)):
		result += curr+7
		found += 1

print(result)
