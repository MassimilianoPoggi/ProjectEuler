from math import sqrt
import itertools

def is_prime(n):
	if n == 1:
		return False
	if n == 2:
		return True

	for i in range(2, int(sqrt(n))+1):
		if (n % i == 0):
			 return False
	return True

def is_circular(num):
	for i in range(0, len(num)):
		if (not is_prime(int(num[i:]+num[:i]))):
			return False
	return True

def check_digits(str):
	for c in str:
		if c not in ['1', '3', '7', '9']:
			return False
	return True

#check fails for 2 and 5, the only circular primes containing such digits
count = 2
for i in range(3, 1000000, 2):
	curr = str(i)
	if (not check_digits(curr)):
		continue

	if (is_circular(curr)):
		count += 1

print(count)
