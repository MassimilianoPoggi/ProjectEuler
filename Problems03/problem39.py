from copy import copy

def gen_triple(m, n):
	return tuple(sorted([m**2 - n**2, 2*m*n, m**2+n**2]))

def mcd(m, n):
	if n == 0:
		return m
	return mcd(n, m%n)

def perimeter(sides):
	sum = 0
	for side in sides:
		sum += side
	return sum

def generate_primitive_triples():
	result = set([])
	for m in range(1, 33):
		for n in range(1, m):
			if ((m - n) % 2 == 1 and mcd(m, n) == 1):
				triple = gen_triple(m, n)
				if (perimeter(triple) <= 1000):
					result.add(triple)
	return set(result)

def scalar_mult(v, n):
	return tuple([n*x for x in v])

def expand_triples(triples, max):
	result = copy(triples)
	for triple in triples:
		i = 2
		new = scalar_mult(triple, i)
		while (perimeter(new) <= max):
			result.add(new)
			i += 1
			new = scalar_mult(triple, i)
	return result

triples = expand_triples(generate_primitive_triples(), 1000)

max = 0
maxp = 0
for p in range(12, 1001):
	count = len(list(filter(lambda x: perimeter(x) == p, triples)))
	if count > max:
		max = count
		maxp = p
print(maxp)
