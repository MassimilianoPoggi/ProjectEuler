def palindrome(s):
	if (len(s) <= 1):
		return True
	if (s[0] != s[-1]):
		return False
	else:
		return palindrome(s[1:-1])

count = 0
for i in range(1, 1000000):
	if (palindrome(str(i)) and palindrome(bin(i)[2:])):
		count += i

print(count)
