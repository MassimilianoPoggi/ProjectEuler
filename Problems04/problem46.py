#Miller-Rabin primality test
from random import randrange

small_primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31] # etc.

def probably_prime(n, k):
    """Return True if n passes k rounds of the Miller-Rabin primality
    test (and is probably prime). Return False if n is proved to be
    composite.

    """
    if n < 2: return False
    for p in small_primes:
        if n < p * p: return True
        if n % p == 0: return False
    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in range(k):
        a = randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True


#problem
def is_prime(n):
        return probably_prime(n, 7)

squares = [1, 4, 9]
num = 9
square_gen = 4
found = False
while(True):
	if (is_prime(num)):
		num += 2
		continue

	while(num >= 2 * square_gen**2):
		squares.append(square_gen**2)
		square_gen += 1

	found = True
	for square in squares:
		if (is_prime(num - 2*square)):
			found = False
			break

	if (found):
		print(num)
		break

	num += 2
