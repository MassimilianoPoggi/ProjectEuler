def triangular(index):
	return index*(index+1)//2

def pentagonal(index):
	return index*(3*index-1)//2

def hexagonal(index):
	return index*(2*index-1)

def search_pentagonal(num, pindex):
	x = pentagonal(pindex)
	while (x < num):
		pindex += 1
		x = pentagonal(pindex)
	return pindex

def search_hexagonal(num, hindex):
	x = hexagonal(hindex)
	while(x < num):
		hindex += 1
		x = hexagonal(hindex)
	return hindex

tindex = 285
hindex = 143
pindex = 165

while(True):
	tindex += 1
	n = triangular(tindex)
	pindex = search_pentagonal(n, pindex)
	if (n == pentagonal(pindex)):
		hindex = search_hexagonal(n, hindex)
		if (n == hexagonal(hindex)):
			print(n)
			break
