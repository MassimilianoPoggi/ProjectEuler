num = ""
length = 0
curr = 0
while (length < 10**6):
	curr += 1
	length += len(str(curr))
	num += str(curr)


result = 1
for i in range(0, 6):
	result *= int(num[10**i-1])

print(result)
