#Miller-Rabin primality test
from random import randrange

small_primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31] # etc.

def probably_prime(n, k):
    """Return True if n passes k rounds of the Miller-Rabin primality
    test (and is probably prime). Return False if n is proved to be
    composite.

    """
    if n < 2: return False
    for p in small_primes:
        if n < p * p: return True
        if n % p == 0: return False
    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in range(k):
        a = randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True


#problem
def is_prime(n):
        return probably_prime(n, 7)


from itertools import permutations

def perms(n):
	 return list(set(sorted(map(lambda x: int("".join(x)), permutations(str(n))))))

def arith_sequences(num):
	result = []
	lst = perms(num)
	for num in lst:
		for num1 in lst:
			if (num1 <= num):
				 continue

			diff = num1 - num

			if ((num1 + diff) in lst):
				result.append([num, num1, num1+diff])

	return result

for i in range(1000, 10000):
	if (str(i) != "".join(sorted(str(i)))):
		continue

	for seq in arith_sequences(i):
		found = True
		for n in seq:
			if (not is_prime(n)):
				found = False
				break
		if (found and i != 1478):
			print("".join(map(lambda x: str(x), seq)))
