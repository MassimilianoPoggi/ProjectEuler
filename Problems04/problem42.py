res = open("problem42.txt", "r")
text = res.read()
res.close()

words = list(map(lambda x: x[1:-1], text.split(",")))

def word_val(word):
	sum = 0
	for c in word:
		sum += ord(c) - ord('A')+1
	return sum

def gen_triangulars(max):
	sum = 0
	curr = 1
	result = []
	while (sum < max):
		sum+=curr
		curr+=1
		result.append(sum)
	return result

max_triangular = word_val("Z") * max(map(lambda x: len(x), words))
triangulars = gen_triangulars(max_triangular)

count = 0
for word in words:
	if (word_val(word) in triangulars):
		count += 1
print(count)
